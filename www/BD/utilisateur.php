<?php

  function statutAll(){
    return ["Etudiant", "Professeur", "Personnel", "administrateur"];
  }

  function InfoUtil($email){
    return array('Nom' => "John", 'Prenom' => "Doe", 'Statut' => "Etudiant", 'Email' => "John.Doe@etu.univ-orleans.fr", 'Departement' => "Informatique", 'img' => "./image/iconUser/iconUser.png");
  }

  function ModifNomUtil($Nom){
    return true;
  }
  function ModifAvatarUtil($Avatar){
    return true;
  }
  function ModifPrenomUtil($Prenom){
    return true;
  }
  function ModifEmailUtil($Email){
    return true;
  }
  function ModifStatutUtil($Statut){
    return true;
  }
  function ModifDepartementUtil($Departement){
    return true;
  }

  function ImgUtil($email){
    return "./image/iconUser/iconUser.png";
  }

  function ticketsUtil($email){
    return [
        array(
            "Date" => "12/10/2021",
            "Nom" => "probleme ecran",
            "Status" => "En cours",
        ),
        array(
          "Date" => "01/10/2021",
          "Nom" => "probleme ecran",
          "Status" => "En cours",
        ),
        array(
          "Date" => "15/09/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "23/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "18/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "12/04/2021",
          "Nom" => "probleme ecran",
          "Status" => "En suspens",
        )
      ];
  }

  function ticketsUtilAll($email){
    return [
        array(
            "Date" => "12/10/2021",
            "Nom" => "probleme ecran",
            "Status" => "En cours",
        ),
        array(
          "Date" => "01/10/2021",
          "Nom" => "probleme ecran",
          "Status" => "En cours",
        ),
        array(
          "Date" => "15/09/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "23/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "18/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "30/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "18/06/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "28/04/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "18/02/2021",
          "Nom" => "probleme ecran",
          "Status" => "Traité",
        ),
        array(
          "Date" => "12/04/2021",
          "Nom" => "probleme ecran",
          "Status" => "En suspens",
        )

      ];
  }
?>
