<?php
declare(strict_types=1);

namespace ProjetAnomalies\GlpiApi;

use Glpi\Api\Rest\ItemHandler;
use ProjetAnomalies\GlpiApi\Exceptions\RequestException;
use StdClass;

/**
 * This is an helper to parse search options.
 */
class SearchOptions
{
    private StdClass $searchOptions;

    /**
     * Get the search option for the given itemType.
     *
     * @param ItemHandler $itemHandler The handler.
     * @param string $itemType The item type.
     * @throws RequestException
     */
    public static function getForItemType(ItemHandler $itemHandler, string $itemType): SearchOptions
    {
        $requestRes = $itemHandler->listSearchOptions($itemType);
        if ($requestRes["statusCode"] == 200) {
            $searchOptions = json_decode($requestRes["body"]);
            $res = new SearchOptions();

            $res->searchOptions = $searchOptions;

            return $res;
        } else {
            $content = json_decode($requestRes["body"]);
            throw new RequestException($content[1], $content[0], $requestRes["statusCode"]);
        }
    }

    /**
     * Get a field from his name.
     *
     * @param string $name
     * @return string|null
     */
    public function getFieldForName(string $name): ?string
    {
        foreach ($this->searchOptions as $key => $option) {
            if (gettype($option) == "object" && property_exists($option, "field") && $option->field == $name) {
                return $key;
            }
        }

        return null;
    }

    /**
     * UID mean Unique ID. However, multiple field can have the same UID, because why not ¯\_(ツ)_/¯.
     *
     * @param string $uid
     * @return string|null
     */
    public function getFieldForUID(string $uid): ?string
    {
        foreach ($this->searchOptions as $key => $option) {
            if (gettype($option) == "object" && property_exists($option, "uid") && $option->uid == $uid) {
                return $key;
            }
        }

        return null;
    }

    /**
     * Get the field from his ID.
     *
     * @param string $fieldId
     * @return StdClass
     */
    public function getField(string $fieldId): StdClass
    {
        return $this->searchOptions[$fieldId];
    }
}