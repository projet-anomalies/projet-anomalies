<?php
declare(strict_types=1);

namespace ProjetAnomalies\GlpiApi;

use Exception;
use phpDocumentor\Reflection\Location;
use stdClass;

use Glpi\Api\Rest\Client;
use Glpi\Api\Rest\ItemHandler;
use ProjetAnomalies\GlpiApi\Exceptions\RequestException;

/**
 * This class dispatch requests to the good instance.
 */
class ClientDispatcher
{
    private array $handlers;

    public function __construct()
    {
        $this->handlers = [];
    }

    /**
     * Load instances from config file.
     *
     * @param string $path The config path.
     * @throws Exception If it fail to connect to an instance or fail to load the file.
     */
    public static function initFromConfigFile(string $path): ClientDispatcher
    {
        $file_content = file_get_contents($path);// Open the file.

        if ($file_content == false) {
            throw new Exception("file not found");
        }

        $config = json_decode($file_content); // Decode the config.
        $dispatcher = new ClientDispatcher();

        // Connect to all instances
        foreach ($config->api_client as $client_config) {
            $client = new Client($client_config->api_url, new \GuzzleHttp\Client());
            $client->setAppToken($client_config->app_token);
            $client->initSessionByUserToken($client_config->user_token);

            $dispatcher->handlers[$client_config->id] = new ItemHandler($client);
        }

        return $dispatcher;
    }

    /**
     * Get a ticket from the id.
     *
     * @param string $instance The ID of the instance.
     * @param string $id The ID of the ticket.
     * @param array $queryString An array to pass params to GLPI.
     * @throws RequestException
     */
    public function getTicket(string $instance, string $id, array $queryString = []): StdClass
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->getItem("Ticket", $id, $queryString);

        if ($res["statusCode"] == 200) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Get all tickets. Specify the params in queryString.
     *
     * @param string $instance The ID of the instance.
     * @param array $queryString An array to pass params to GLPI.
     * @throws RequestException
     */
    public function getTickets(string $instance, array $queryString = []): array
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->getAllItems("Ticket", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            var_dump($content);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Create a ticket in GLPI.
     *
     * @param string $instance The ID of the instance.
     * @param array $params An array to pass params to GLPI.
     * @throws RequestException
     */
    public function addTicket(string $instance, array $params): StdClass
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->addItems("Ticket", $params);
        if ($res["statusCode"] == 201) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Update a ticket.
     *
     * @param string $instance The ID of the instance.
     * @param int $id The ID of the ticket to edit.
     * @param array $params An array to pass params to GLPI.
     * @throws RequestException
     */
    public function updateTicket(string $instance, int $id, array $params = []): array
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->updateItems("Ticket", $id, $params);
        if ($res["statusCode"] == 200) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Get all the handlers in this dispatcher.
     *
     * @return Client[]
     */
    public function getHandlers(): array
    {
        return $this->handlers;
    }

    /**
     * Get the client associated to this ID.
     *
     * @param string $instance The instance id.
     * @return Client The client bound to this instance.
     */
    public function getClient(string $instance): Client
    {
        return $this->handlers[$instance]->getClient();
    }

    /**
     * Get the handler associated to this ID.
     *
     * @param string $instance The instance id.
     * @return ItemHandler The handler bound to this instance.
     */
    public function getItemHandler(string $instance): ItemHandler
    {
        return $this->handlers[$instance];
    }

    /**
     * Fetch the search options for the itemType.
     *
     * @param string $instance The instance id.
     * @param string $itemType The item type.
     * @param array $params An array to pass params to GLPI.
     * @throws RequestException
     */
    public function getSearchOptions(string $instance, string $itemType): SearchOptions
    {
        return SearchOptions::getForItemType($this->handlers[$instance], $itemType);
    }

    /**
     * Search items in GLPI.
     *
     * @param string $instance The instance ID.
     * @param string $itemType The type of item to search.
     * @param array $params An array to pass params to GLPI.
     * @throws RequestException
     */
    public function searchItems(string $instance, string $itemType, array $queryString): StdClass
    {
        $res = $this->handlers[$instance]->searchItems($itemType, $queryString);
        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Get all categories from GLPI.
     *
     * @param string $instance The instance ID.
     * @param array $params An array to pass params to GLPI.
     * @throws RequestException
     */
    public function getCategories(string $instance, array $queryString = []): array
    {
        $res = $this->handlers[$instance]->getAllItems("ITILCategory", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Get locations from GLPI.
     *
     * @param string $instance The instance ID.
     * @param array $queryString An array to pass params to GLPI.
     * @throws RequestException
     */
    public function getLocations(string $instance, array $queryString = []): array
    {
        $res = $this->handlers[$instance]->getAllItems("Location", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * Search the location ID from the name.
     *
     * @param string $instance The instance ID.
     * @param string $locationName The name of the location.
     * @param SearchOptions|null $locationSearchOptions The search options, or null. Using a cached search option should improve performances.
     * @throws RequestException
     */
    public function getLocationId(string $instance, string $locationName, ?SearchOptions $locationSearchOptions = null): ?string
    {
        if ($locationSearchOptions == null) {
            $locationSearchOptions = $this->getSearchOptions($instance, "Location");
        }

        $nameFieldId = $locationSearchOptions->getFieldForName("name");

        $res = $this->searchItems("sidev", "Location", [
            "criteria" => [[
                "field" => $nameFieldId,
                "searchtype" => "contains", // ALED, EQUALS IS BY ID OF STRING BECAUSE WHY THE FUCK NOT.
                "value" => $locationName
            ]],
            "withindexes" => true
        ]);

        foreach ($res->data as $id => $value) {
            if (((array)$value)[$nameFieldId] == $locationName) {
                return $id;
            }
        }

        return null;
    }

    /**
     * Featch assets in a location.
     *
     * @param string $instance The instance ID.
     * @param string $locationId The id of the location.
     * @param SearchOptions|null $locationSearchOptions The search options, or null. Using a cached search option should improve performances.
     * @throws RequestException
     */
    public function getAssetsInLocation(string $instance, string $locationId, ?SearchOptions $computerSearchOptions = null): StdClass
    {
        if ($computerSearchOptions == null) {
            $computerSearchOptions = $this->getSearchOptions($instance, "Computer");
        }

        return $this->searchItems("sidev", "AllAssets", [
            "criteria" =>
                [[
                    "field" => $computerSearchOptions->getFieldForName("completename"),
                    "searchtype" => 'equals',
                    "value" => $locationId
                ]],
        ]);
    }
}