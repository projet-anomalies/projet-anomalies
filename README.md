# Project Anomalie

[![GitLab issues open](https://badgen.net/gitlab/open-issues/projet-anomalies/projet-anomalies)](https://gitlab.com/projet-anomalies/projet-anomalies)

# Prérequis
- Composer
- Symfony

# Installation
```bash
cd Symfony
echo "DATABASE_URL=sqlite3:///db.db3" >> .env
composer i
```

# Lancer
Dans le dossier Symfony
```bash
symfony server:start --no-tls
```
