<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ScanController extends AbstractController
{

    /**
     * @Route("/scanner")
     */
    public function scanner(): Response
    {

       return $this->render('scan.html.twig');

    }
}
