console.log('lancement...');

/**
 * 
 * @returns {Object} The dictionnay of the parameters necessary for the QRCode
 */
const getParams = () => {
    /**
     * @return {Array}  The list of params from the form
     */
    let params = {
        "departement" : "",
        "salle" : "",
        "type" : "",
        "nom" : ""
      };
    params["departement"] = document.getElementById('select-departement').value;
    params["salle"] = document.getElementById('select-salle').value;
    
    let p = document.getElementById('select-name');
    params["type"] = p[p.selectedIndex].id;
    params["nom"] = p.value;
    

    return params;
}

/**
 * Generate and display the QRCode.
 */
 async function makeCode() {
    // Get the value from the fields
    const params = getParams();
    const divQRCode = document.getElementById('qrcode');

    // Remove all child if present
    if (divQRCode.hasChildNodes()) { 
        divQRCode.innerHTML = '';
    }

    // information of qrcode
    ajouterInformations(divQRCode);
    
    // Generation of qrcode
    let qrcode = new QRCode("qrcode");
    qrcode.makeCode(makeLien(params));
    displayBtn("btn-print");
}

/**
 * Add the information corresponding to the QRCode.
 * @param  {Node} divQRCode  The node aimed to contain the information
 */ 
const ajouterInformations = (divQRCode) => {

    // Retrieve informations
    let departementSelect = document.getElementById('select-departement');
    let salleSelect = document.getElementById('select-salle');
    let materielSelect = document.getElementById('select-name');

    let departement = departementSelect[departementSelect.selectedIndex].text;
    let salle = salleSelect[salleSelect.selectedIndex].text;
    let materiel = materielSelect[materielSelect.selectedIndex].text;
    

    // Creation of paragraph section

    let pDep = document.createElement("p");
    let pSalle = document.createElement("p");
    let pNom = document.createElement("p");

    // Add text to paragraph 
    pDep.append("Departement : " + departement);
    pSalle.append("Salle : " + salle);
    pNom.append("Nom : " + materiel);

    // Add to the page
    let container = document.createElement('div');
    container.id = "informations";
    container.appendChild(pDep);
    container.appendChild(pSalle);
    container.appendChild(pNom);
    divQRCode.append(container);
}

/**
 * Add the information corresponding to the QRCode - for all the QRCode.
 * @param  {Node} divQRCode  The node aimed to contain the information
 * @param {AssetGLPI} asset The equipement object
 */ 
 const ajouterInformationsAll = (divQRCode, asset) => {

    // Retrieve informations
    let departementSelect = document.getElementById('select-departement');
    let salleSelect = document.getElementById('select-salle');

    let departement = departementSelect[departementSelect.selectedIndex].text;
    let salle = salleSelect[salleSelect.selectedIndex].text;
    

    // Creation of paragraph section

    let pDep = document.createElement("p");
    let pSalle = document.createElement("p");
    let pNom = document.createElement("p");

    // Add text to paragraph 
    pDep.append("Departement : " + departement);
    pSalle.append("Salle : " + salle);
    pNom.append("Nom : " + asset.name);

    // Add to the page
    let container = document.createElement('div');
    container.appendChild(pDep);
    container.appendChild(pSalle);
    container.appendChild(pNom);
    divQRCode.append(container);
} 

/**
 * Generate and display all the QRCode.
 */   
const makeAllCodes = async () => {

    let departement = document.getElementById('select-departement').value;
    let salle = document.getElementById('select-salle').value;

    let assetList = await GlpiApi.loadAssetsList(salle);

    //
    const divQRCode = document.getElementById('qrcode');

    // Remove all child if present
    if (divQRCode.hasChildNodes()) { 
        divQRCode.innerHTML = '';
    }

    // Build parameters
    let params = {
        "departement" : departement,
        "salle" : salle,
        "type" : "",
        "nom" : ""
      };

      let i = 0;
    // Display each QRCode
    for (let asset of assetList){
        params["type"] = asset.type;
        params["nom"] = asset.name;
        
        // Create a container
        let container = document.createElement('div');
        container.id = "qrcode" + i;
        container.classList.add("mx-2", "mb-4");
        
        // information of qrcode
        ajouterInformationsAll(container, asset);
        divQRCode.append(container);



        // Generation of qrcode
        let qrcode = new QRCode("qrcode" + i);
        qrcode.makeCode(makeLien(params));

        i++;
    }

    displayBtn("btn-print");
}

/**
 * Create the string -> link corresponding to the QRCode.
 * @param  {List}    params  The list of the parameters
 * @return {String}          The link of the QRCode.
 */
const makeLien = (params) => {
    let lien = "http://127.0.0.1:8000/anomalie?departement=" ;
    lien += params["departement"];
    lien += "&salle=";
    lien += params["salle"];
    lien += "&type=";
    lien += params["type"];
    lien += "&nom="
    lien += params["nom"];
    return lien;
}


/**
 * Create the print button and append it into the node.
 * @param  {List}    node  The node aimed to contain the print button
 */
const createPrintButton = (node) => {
    let btn = document.createElement('button');
    btn.id = "btn-print";
    btn.classList.add("btn", "btn-primary");
    btn.onclick = () =>{
        printQR('qrcode');
    };
    btn.append('Imprimer');
    node.appendChild(btn);
}

/**
 * Make the corresponding button displayed on the screen (by removing bootstrap class)
 * @param  {String} id  The id of the button to make visible
 */
const displayBtn = (id) => {
    document.getElementById(id).classList.remove('d-none');
}
