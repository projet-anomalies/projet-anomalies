class Source {
    constructor(type) {
        this._type = type;
        this._ListReason = [];
    }

    toString() {
        return '${this._type}';
    }

    addReason(val) {
        this._ListReason.push(val);
    }

    get reason() {
        return this._ListReason;
    }

    get type() {
        return this._type;
    }
}


class Option {
    constructor(name, value) {
        this._values = value;
        this._name = name;
    }

    toString() {
        return '${this._name}';
    }

    addSalle(val) {
        this._listObject.push(val);
    }

    get value() {
        return this._values;
    }

    get name() {
        return this._name;
    }

}

const list_source_ordi = [];
const list_source_autre = [];

class AppForm {
    constructor() {
        this._ticketEmail = document.getElementById('ticket_email');
        this._ticketDepartement = document.getElementById('ticket_departement');
        this._ticketSalle = document.getElementById('ticket_salle');
        this._ticketType = document.getElementById('ticket_type');
        this._ticketNom = document.getElementById('ticket_nom');
        this._ticketSource = document.getElementById('ticket_source');
        this._ticketProblem = document.getElementById("ticket_problem");
        this._ticketDescription = document.getElementById("ticket_description");

        const self = this;

        this._ticketEmail.onchange = (e) => self.checkEmail(e);
        this._ticketDepartement.onchange = (e) => self.updateSalle(e);
        this._ticketSalle.onchange = () => self.updateType();
        this._ticketType.onchange = (e) => self.updateNom(e);
        this._ticketNom.onchange = (e) => self.updateSource(e);
        this._ticketSource.onchange = (e) => self.updateInfo(e);
        this._ticketProblem.onchange = (e) => self.updateDescription(e);

    }

    async load() {
        //fil departement
        if(!qrCode){
          let optionDepartement = []
          let departement = await GlpiApi.loadBuildingsList();
          for (let i = 0; i < departement.length; i++) {
              optionDepartement.push(new Option(departement[i].name, departement[i].idDep));
          }
          this.fillSelect(this._ticketDepartement, optionDepartement);


          this.resetSalle();
          this.resetType();
          this.resetNom();
        }else{
          let depart = document.getElementById("valDepart");
          let salle = document.getElementById("valSalle");
          let nom = document.getElementById("valNom");

          let nameDepart = await GlpiApi.loadNameRoom(depart.value);
          let nameSalle = await GlpiApi.loadNameRoom(salle.value);
          let nameNom = await GlpiApi.loadNameAppareil(nom.value, salle.value);

          depart.text = nameDepart.slice(0, -11);
          salle.text = nameSalle;
          nom.text = nameNom;

          this.updateSourceAnomalie();

        }
        this.bindSourceAnomalie();
        this.resetProblem();
    }


    /**
     * Remplis un select avec la liste des option du select la value.
     *
     * @param {HTMLElement} element
     * @param {*[]} list
     */
    fillSelect(element, list) {
        element.innerHTML = '';
        let option = document.createElement("option");

        option.text = "-- choisissez --";
        element.appendChild(option);

        for (let i = 0; i < list.length; i++) {
            option = document.createElement("option");

            const value = list[i].value;
            const name = list[i].name;

            option.setAttribute("value", value);
            option.text = name;
            element.appendChild(option);
        }
    }

    removeImportant(){
      const listImport = document.getElementsByClassName("important");
      for(let i = 0; i < listImport.length; i++){
        listImport[i].classList.remove("important");
      }
    }


    fixeValue(){
      if(departFix){
          option = document.getElementById("valDepart");
          console.log( option.value);
      }
      if(salleFix){
          option = document.getElementById("valSalle");
          console.log( option.value);
      }
      if(nomFix){
          option = document.getElementById("valNom");
          console.log( option.value);

      }
    }

    bindSourceAnomalie() {
        const type = this._ticketType.value;
        if ("Computer" === type) {
            document.getElementById("sectionBinding").style.display = "block";
            return true;
        } else {
            document.getElementById("sectionBinding").style.display = "none";
            return false;
        }
    }

    resetSalle() {
        if(!salleFix){
            this.fillSelect(this._ticketSalle, this.transformListInOption([]));
        }
    }

    resetType() {
        if(!typeFix){
            this.fillSelect(this._ticketType, this.transformListInOption([]));
            this.bindSourceAnomalie(source);
        }
    }

    resetNom() {
        if(!nomFix){
            this.fillSelect(this._ticketNom, this.transformListInOption([]));
        }
    }

    resetProblem() {
        this.fillSelect(this._ticketProblem, this.transformListInOption([]));
    }


    checkEmail(element) {
        console.log("checkEmail")
        let enteredEmail = element.target.value;
        const mail_format = /^[a-z]+\.[a-z]+@(?:[a-z]+\.)?univ-orleans\.fr$/;

        let spanMail = document.getElementById("emailHelp");
        if (!enteredEmail.endsWith('univ-orleans.fr')) {
          spanMail.className = "text-danger";
          spanMail.innerText = "Attention l'adresse mail ne fini pas par univ-orleans.fr";
          return false;
        }
        if (enteredEmail.match(mail_format)) {

              spanMail.className = "text-success";
              spanMail.innerText = "adresse mail valide";
              this.removeImportant();
              if(!departFix){
                this._ticketDepartement.classList.add("important");
              }else{
                this.updateSource();
              }
              return true;
        }
        spanMail.className = "text-danger";
        spanMail.innerText = "attention ce n'est pas une adresse mail non valide il faut le nom et le prenom separé par un . ,un aprés @ et qu'elle finissent par univ-orleans.fr";
        return false;
    }


    async updateSalle(element) {
        if(!salleFix){
            console.log("update Salle");
            let source = element.target.value;
            let optionSalle = []
            let salle = await GlpiApi.loadRoomList(source);
            for (let i = 0; i < salle.length; i++) {
                optionSalle.push(new Option(salle[i].name, salle[i].idSalle));
            }

            this.fillSelect(this._ticketSalle, optionSalle);
            this.resetType();
            this.resetNom();
            this.resetProblem();
            this.bindSourceAnomalie(source);
            this.removeImportant();
            this._ticketSalle.classList.add("important")
        }
    }

    async updateType() {
        if(!typeFix){
            console.log("update Type");
            let optionType = []
            let option = []
            let source = document.getElementById("ticket_salle").value;
            let objet = await GlpiApi.loadAssetsList(source);
            for (let i = 0; i < objet.length; i++) {
                if (option.indexOf(objet[i].type) !== -1) {

                } else {

                    option.push(objet[i].type)
                    optionType.push(new Option(objet[i].type, objet[i].type));
                }
            }
            this.fillSelect(this._ticketType, optionType);
            this.updateSourceAnomalie();
            this.resetNom();
            this.resetProblem();
            this.bindSourceAnomalie(source);
            this.removeImportant();
            this._ticketType.classList.add("important")
        }
    }


    async updateNom(element) {
        if(!nomFix){
            console.log("update Nom");
            let type = element.target.value;
            let source = document.getElementById("ticket_salle").value;
            let optionNom = []
            let nom = await GlpiApi.loadAssetsList(source);
            for (let i = 0; i < nom.length; i++) {
                if (nom[i].type === type) {
                    optionNom.push(new Option(nom[i].name, nom[i].idObj));
                }
            }

            this.fillSelect(this._ticketNom, optionNom);
            this.bindSourceAnomalie(source);
            this.removeImportant();
            this._ticketNom.classList.add("important")
        }
    }


    updateSourceAnomalie() {
        let optionType = [];
        for (const element of list_source_ordi) {
            optionType.push(new Option(element.type, element.type));
        }
        this.fillSelect(this._ticketSource, optionType);
    }


    updateInfo(element){
      console.log("update info");
      if(this.bindSourceAnomalie()){
        this.updateInfoAnomalieOrdi(element);
        console.log("llaaa");
        this.removeImportant();
        this._ticketProblem.classList.add("important");
      }else{
        this.updateInfoAnomalieAutre();
      }
    }

    updateInfoAnomalieOrdi(element) {
        let source = element.target.value;
        for (const e of list_source_ordi) {
            if (e.type === source) {
                this.fillSelect(this._ticketProblem, this.transformListInOption(e.reason));
            }
        }
    }


    updateInfoAnomalieAutre() {
        this.fillSelect(this._ticketProblem, this.transformListInOption(list_source_autre));
    }

    transformListInOption(list) {
        let res = [];
        for (const val of list) {
            res.push(new Option(val, val));
        }
        return res;
    }

    updateSource(element){
      this.removeImportant();
      //this.updateInfo(element);
      console.log("update source");
      if(this.bindSourceAnomalie()){
        this.removeImportant();
        this._ticketSource.classList.add("important");
      }else{
        console.log("elui la");
        this.updateInfoAnomalieAutre();
        this._ticketProblem.classList.add("important");
      }
    }

    updateDescription(element){
      console.log("update description");
      this.removeImportant();
      this._ticketDescription.classList.add("important")
    }
}

window.onload = async () => {
    const app = new AppForm();

    await app.load();
}
